

#ifndef _ASM_REGDEF_H
#define _ASM_REGDEF_H

#ifndef ARCH_MIPS64
#define zero    $0  /* wired zero */
#define AT  $1  /* assembler temp  - uppercase because of ".set at" */
#define v0  $2  /* return value */
#define v1  $3
#define a0  $4  /* argument registers */
#define a1  $5
#define a2  $6
#define a3  $7
#define t0  $8  /* caller saved */
#define t1  $9
#define t2  $10
#define t3  $11
#define t4  $12
#define ta0 $12
#define t5  $13
#define ta1 $13
#define t6  $14
#define ta2 $14
#define t7  $15
#define ta3 $15
#define s0  $16 /* callee saved */
#define s1  $17
#define s2  $18
#define s3  $19
#define s4  $20
#define s5  $21
#define s6  $22
#define s7  $23
#define t8  $24 /* caller saved */
#define t9  $25
#define jp  $25 /* PIC jump register */
#define k0  $26 /* kernel scratch */
#define k1  $27
#define gp  $28 /* global pointer */
#define sp  $29 /* stack pointer */
#define fp  $30 /* frame pointer */
#define s8  $30 /* same like fp! */
#define ra  $31 /* return address */

#else

#define zero    $0  /* wired zero */
#define AT  $at /* assembler temp - uppercase because of ".set at" */
#define v0  $2  /* return value - caller saved */
#define v1  $3
#define a0  $4  /* argument registers */
#define a1  $5
#define a2  $6
#define a3  $7
#define a4  $8  /* arg reg 64 bit; caller saved in 32 bit */
#define ta0 $8
#define a5  $9
#define ta1 $9
#define a6  $10
#define ta2 $10
#define a7  $11
#define ta3 $11
#define t0  $12 /* caller saved */
#define t1  $13
#define t2  $14
#define t3  $15
#define s0  $16 /* callee saved */
#define s1  $17
#define s2  $18
#define s3  $19
#define s4  $20
#define s5  $21
#define s6  $22
#define s7  $23
#define t8  $24 /* caller saved */
#define t9  $25 /* callee address for PIC/temp */
#define jp  $25 /* PIC jump register */
#define k0  $26 /* kernel temporary */
#define k1  $27
#define gp  $28 /* global pointer - caller saved for PIC */
#define sp  $29 /* stack pointer */
#define fp  $30 /* frame pointer */
#define s8  $30 /* callee saved */
#define ra  $31 /* return address */

#endif

#define fv0 $f0  /* return value */
#define fv0f    $f1
#define fv1 $f2
#define fv1f    $f3
#define fa0 $f12     /* argument registers */
#define fa0f    $f13
#define fa1 $f14
#define fa1f    $f15
#define ft0 $f4  /* caller saved */
#define ft0f    $f5
#define ft1 $f6
#define ft1f    $f7
#define ft2 $f8
#define ft2f    $f9
#define ft3 $f10
#define ft3f    $f11
#define ft4 $f16
#define ft4f    $f17
#define ft5 $f18
#define ft5f    $f19
#define fs0 $f20     /* callee saved */
#define fs0f    $f21
#define fs1 $f22
#define fs1f    $f23
#define fs2 $f24
#define fs2f    $f25
#define fs3 $f26
#define fs3f    $f27
#define fs4 $f28
#define fs4f    $f29
#define fs5 $f30
#define fs5f    $f31

#define fcr31   $31  /* FPU status register */


#endif /* _ASM_REGDEF_H */

