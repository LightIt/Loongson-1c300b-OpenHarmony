#include "los_config.h"
#include "los_debug.h"
#include "los_interrupt.h"
#include "los_task.h"
#include "los_tick.h"

#include "ls1c_public.h"
#include "ls1c_gpio.h"
#include "ls1c_delay.h"
#include "ls1c_irq.h"



VOID TaskSampleEntry2(VOID)
{
    printf("___>>>> start task %s\r\n", __FUNCTION__);
    while (1) 
    {
        printf("___>>>>>> TaskSampleEntry2 %s %d\r\n\r\n", __FILE__, __LINE__);
        LOS_TaskDelay(2000);
    }
}

VOID TaskSampleEntry1(VOID)
{
    printf("___>>>> start task %s\r\n", __FUNCTION__);
    while (1) 
    {
        printf("___>>>>>> TaskSampleEntry1 %s %d\r\n\r\n", __FILE__, __LINE__);
        LOS_TaskDelay(500);
    }
}


VOID TaskSample(VOID)
{
    UINT32 uwRet;
    UINT32 taskID1;
    UINT32 taskID2;
    TSK_INIT_PARAM_S stTask = {0};

    stTask.pfnTaskEntry = (TSK_ENTRY_FUNC)TaskSampleEntry1;
    stTask.uwStackSize = 0x1000;
    stTask.pcName = "TaskSampleEntry1";
    stTask.usTaskPrio = 6; /* Os task priority is 6 */
    uwRet = LOS_TaskCreate(&taskID1, &stTask);
    if (uwRet != LOS_OK) {
        printf("Task1 create failed\r\n");
    }

    stTask.pfnTaskEntry = (TSK_ENTRY_FUNC)TaskSampleEntry2;
    stTask.uwStackSize = 0x1000;
    stTask.pcName = "TaskSampleEntry2";
    stTask.usTaskPrio = 7; /* Os task priority is 7 */
    uwRet = LOS_TaskCreate(&taskID2, &stTask);
    if (uwRet != LOS_OK) {
        printf("Task2 create failed\r\n");
    }
}


