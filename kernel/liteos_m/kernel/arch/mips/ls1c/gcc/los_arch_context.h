/*
 * Copyright (c) 2013-2019 Huawei Technologies Co., Ltd. All rights reserved.
 * Copyright (c) 2020-2021 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _LOS_ARCH_CONTEXT_H
#define _LOS_ARCH_CONTEXT_H
 #include "target_config.h"
#include "los_config.h"
#include "los_compiler.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */



#define HI_LO_SIZE  4

#define FP_REG_SIZE 8
#define NUM_FPU_REGS 16

struct mips_fpu_struct {
    UINT64 fpr[NUM_FPU_REGS];
    UINT32 fcr31;
    UINT32 pad;
};

typedef struct TagTskContext {
    #ifndef ARCH_MIPS64
    /* Only O32 Need This! */
    /* Pad bytes for argument save space on the stack. */
    UINT32 pad0[8];

    /* Saved main processor registers. */
    UINT32 regs[32];

    /* Saved special registers. */
    UINT32 cp0_status;
    UINT32 hi;
    UINT32 lo;
    UINT32 cp0_badvaddr;
    UINT32 cp0_cause;
    UINT32 cp0_epc;
#else
    /* Saved main processor registers. */
    unsigned long regs[32];

    /* Saved special registers. */
    UINT32 cp0_status;
    UINT32 hi;
    UINT32 lo;
    unsigned long cp0_badvaddr;
    UINT32 cp0_cause;
    unsigned long cp0_epc;
#endif

#ifdef MIPS_USING_FPU
    /* FPU Registers */
    /* Unlike Linux Kernel, we save these registers unconditionally,
     * so it should be a part of pt_regs */
    struct mips_fpu_struct fpu;
#endif

} TaskContext;

/**
 * @ingroup  los_config
 * @brief: Task start running function.
 *
 * @par Description:
 * This API is used to start a task.
 *
 * @attention:
 * <ul><li>None.</li></ul>
 *
 * @param: None.
 *
 * @retval None.
 *
 * @par Dependency:
 * <ul><li>los_config.h: the header file that contains the API declaration.</li></ul>
 * @see None.
 */
//extern VOID HalStartToRun(VOID *cur, VOID *new);
extern VOID HalStartToRun(VOID);

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif /* _LOS_ARCH_CONTEXT_H */
