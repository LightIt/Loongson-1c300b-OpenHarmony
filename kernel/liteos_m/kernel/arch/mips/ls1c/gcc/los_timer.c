/*
 * Copyright (c) 2013-2019 Huawei Technologies Co., Ltd. All rights reserved.
 * Copyright (c) 2020-2021 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "los_timer.h"
#include "los_config.h"
#include "los_tick.h"
#include "los_arch_interrupt.h"
#include "los_context.h"
#include "los_sched.h"
#include "los_debug.h"

#include "los_mipsregs.h"


static volatile UINT64 tick_cnt;
static volatile UINT64 tick_reload;

extern void sys_tick_init(unsigned int tick);
extern void sys_tick_enable(void);

extern void sys_tick_disable(void);
extern void sys_tick_enable(void);
extern void sys_tick_set_handle(void (*handle)(void));

void (*g_mips_tick_handle)(void);
void mips_tick_set_handle(void (*handle)(void))
{
    g_mips_tick_handle = handle;
}


/* ****************************************************************************
Function    : HalTickStart
Description : Configure Tick Interrupt Start
Input       : none
output      : none
return      : LOS_OK - Success , or LOS_ERRNO_TICK_CFG_INVALID - failed
**************************************************************************** */
WEAK UINT32 HalTickStart(OS_TICK_HANDLER handler)
{
#if 1

    if ((OS_SYS_CLOCK == 0) ||
        (LOSCFG_BASE_CORE_TICK_PER_SECOND == 0) ||
        (LOSCFG_BASE_CORE_TICK_PER_SECOND > OS_SYS_CLOCK)) {
        return LOS_ERRNO_TICK_CFG_INVALID;
    }

   HalOSHwiCreate(LS1C_TICK_IRQ, 0, 0, (HWI_PROC_FUNC)handler, NULL);

    g_sysClock = OS_SYS_CLOCK;
    g_cyclesPerTick = OS_SYS_CLOCK / LOSCFG_BASE_CORE_TICK_PER_SECOND;

   sys_tick_init(1000);

   sys_tick_enable();
#endif
    return LOS_OK;
}

WEAK VOID HalSysTickReload(UINT64 nextResponseTime)
{
    write_c0_compare((nextResponseTime + 1) );

    write_c0_count(0);
    
    tick_cnt = 0;
}

WEAK UINT64 HalGetTickCycle(UINT32 *period)
{
    UINT32 cout;
    
    *period = read_c0_compare();

    cout = read_c0_count();
    
    return tick_cnt + cout;
}

WEAK VOID HalTickLock(VOID)
{
    sys_tick_disable();
}

WEAK VOID HalTickUnlock(VOID)
{
    sys_tick_enable();
}

VOID platform_tick_handler(VOID)
{
    unsigned int count, cout;

    cout = read_c0_count();
    tick_cnt += cout;
    
    count = read_c0_compare();
    write_c0_compare(count);
    write_c0_count(0);
}


LITE_OS_SEC_TEXT VOID wfi(VOID)
{
    //__asm__ __volatile__("wfi");
}

UINT32 HalEnterSleep(VOID)
{
    wfi();

    return LOS_OK;
}


